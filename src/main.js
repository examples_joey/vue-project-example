import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faMapMarkerAlt, faPlusCircle, faBatteryEmpty, faBatteryHalf, faBatteryFull, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import App from './App'
import store from './store'
import i18n from './i18n'

/**
 * @description
 * -----------------------------------------------------------------------------
 * 共用樣式引入
 * -----------------------------------------------------------------------------
 */
import 'bootstrap/scss/bootstrap.scss'
import './styles/reset.css'

/**
 * @description
 * -----------------------------------------------------------------------------
 * font-awesome 引入
 * -----------------------------------------------------------------------------
 * @example - https://fontawesome.com/how-to-use/on-the-web/using-with/vuejs
 */
library.add(faMapMarkerAlt, faPlusCircle, faBatteryEmpty, faBatteryHalf, faBatteryFull, faTimes)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
    store,
    i18n,
    render: h => h(App)
}).$mount('#app')
