---
title: 'VUE-專案標準化建構流程'
author: 'Joey-Ren'
---

# VUE 專案標準化建構流程

## 目錄

[TOC]

---

## 事前準備
###### 安裝全域工具
```bash
npm install -g @vue/cli
# OR
yarn global add @vue/cli
```
> 在 mac 環境需使用管理員權限才可進行操作

## 建立專案
```bash
npm cd <directory_path>
vue create <project_name>
npm cd <project_name>
```

## 安裝相依套件
###### 安裝 VUEX
```bash
vue add vuex
```
###### 安裝 i18n
```bash
vue add i18n
```
>安裝問答全部同意即可

###### 安裝 stylelint
```bash
npm install -D stylelint stylelint-config-prettier stylelint-config-sass-guidelines stylelint-config-standard stylelint-order stylelint-scss pre-commit
```

###### 安裝 eslint airbnb
```bash
npm i -D @vue/eslint-config-airbnb eslint-config-airbnb-base eslint-plugin-import
```

###### 安裝 loader
```bash
npm i -D node-sass css-loader sass-loader style-loader postcss-loader autoprefixer
```

##### 安裝 fontawesome
```bash
npm i --save @fortawesome/fontawesome-svg-core
npm i --save @fortawesome/free-solid-svg-icons
npm i --save @fortawesome/vue-fontawesome
```
> 可跳過

## 設定檔
###### .eslintrc.js
```javascript=
module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        '@vue/airbnb',
        'plugin:vue/essential',
        'plugin:vue/strongly-recommended',
        'plugin:vue/recommended',
        'eslint:recommended',
        'airbnb-base'
    ],
    plugins: ['vue'],
    rules: {
        /* common */
        indent: [2, 4, { SwitchCase: 1 }],
        'no-unused-vars': 1,
        'comma-dangle': [2, 'never'],
        'arrow-body-style': 2,
        'global-require': 0,
        'max-len': 0,
        'no-alert': 0,
        // 'no-console': 0,
        'object-curly-newline': 0,
        'no-restricted-globals': 0,
        camelcase: 0,
        'semi': ['error', 'never'],

        /* import */
        'import/newline-after-import': 2,
        'import/prefer-default-export': 1,
        'import/extensions': [2, 'always', {
            js: 'never',
            vue: 'never'
        }],
        'import/no-dynamic-require': 0,
        'import/no-extraneous-dependencies': [2, {
            optionalDependencies: ['test/unit/index.js']
        }],
        /* vue plugin */
        'vue/no-v-html': 0,
        'vue/attributes-order': 0,
        'vue/html-indent': [1, 4],
        'vue/attribute-hyphenation': [2, 'never'],
        'vue/max-attributes-per-line': [
            1,
            {
                singleline: 2,
                multiline: {
                    max: 1,
                    allowFirstLine: true
                }
            }
        ],
        'vue/html-self-closing': [2, { html: { void: 'always' } }],
        'no-underscore-dangle': 0
    },
    parserOptions: {
        parser: 'babel-eslint',
    }
};
```

###### .stylelintrc.json
```json=
{
    "extends": [
        "stylelint-config-standard",
        "stylelint-config-prettier",
        "stylelint-config-sass-guidelines"
    ],
    "plugins": [
        "stylelint-scss",
        "stylelint-order"
    ],
    "rules": {
        "max-nesting-depth": null,
        "no-empty-source": null,
        "no-descending-specificity": null,
        "order/properties-alphabetical-order": null,
        "order/properties-order": [
            "position",
            "top",
            "right",
            "bottom",
            "left",
            "display",
            "align-items",
            "justify-content",
            "float",
            "clear",
            "overflow",
            "overflow-x",
            "overflow-y",
            "margin",
            "margin-top",
            "margin-right",
            "margin-bottom",
            "margin-left",
            "padding",
            "padding-top",
            "padding-right",
            "padding-bottom",
            "padding-left",
            "width",
            "min-width",
            "max-width",
            "height",
            "min-height",
            "max-height",
            "font-size",
            "font-family",
            "font-weight",
            "text-align",
            "text-justify",
            "text-indent",
            "text-overflow",
            "text-decoration",
            "white-space",
            "line-height",
            "color",
            "background",
            "background-position",
            "background-repeat",
            "background-size",
            "background-color",
            "background-clip",
            "border",
            "border-style",
            "border-width",
            "border-color",
            "border-top",
            "border-top-style",
            "border-top-width",
            "border-top-color",
            "border-right",
            "border-right-style",
            "border-right-width",
            "border-right-color",
            "border-bottom",
            "border-bottom-style",
            "border-bottom-width",
            "border-bottom-color",
            "border-left",
            "border-left-style",
            "border-left-width",
            "border-left-color",
            "border-radius",
            "opacity",
            "filter",
            "list-style",
            "outline",
            "visibility",
            "z-index",
            "box-shadow",
            "text-shadow",
            "resize",
            "transition",
            "cursor"
        ],
        "property-no-vendor-prefix": null,
        "selector-max-compound-selectors": null,
        "scss/at-import-partial-extension-blacklist": null,
        "value-no-vendor-prefix": null,
        "font-family-no-missing-generic-family-keyword": null,
        "declaration-block-semicolon-newline-after": null,
        "indentation": [4, { "severity": "warning" } ]
    }
}
```

###### cSpell.json
```json=
{
    "words": [
        "yoyo",
        "gsap",
        "ipad",
        "coord",
        "fontawsome",
        "fortawesome",
        "pixi",
        "camelcase",
        "stylelint",
        "vuex",
        "browserslist",
        "fontawesome",
        "fortawesome",
        "redbag",
        "previouspage",
        "bbin",
        "linkid",
        "bbnews",
        "pageview",
        "hotjar",
        "hjid",
        "hjsv",
        "stylesheet",
        "searchfield",
        "textfield",
        "scrollbar"
    ]
}
```
> 使用 Code Spell Checker 才需要進行該設定

## Vuex 設置
###### src/store.js
```javascript=
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        InitData: {
            data: {},
            i18n: tw,
            event: {},
            game: []
        },
        ImgLang: 'zh-tw',
        RouterName: '',
        Device: 'desktop',
        EditWindowData: {},
        EditSetting: {},
        EditWindowOkBtnEnabled: true,
        DefaultSetting: {},
        Alert: {},
        RouterLoadingStatus: true
    },
    getters: {
        getInitData: state => state.InitData,
        getI18n: state => state.InitData.i18n,

        // ----------------- ⇩測試環境用 Development dev_01_03⇩ -----------------
        // 【說明】測試環境模擬 getImgPath 所使用，將上下兩段AB程式碼替換即可

        // 【A】測試環境
        getImgPath: (state) => {
            if (!state.InitData.getImgPath || !state.InitData.upload_folder || !state.RouterName) return 'http://13.115.204.5/product_event/bb_2019_20anniversary/images/slim';

            return `${state.InitData.img_path}/tpl/promo/${state.InitData.upload_folder}/${state.RouterName !== 'view' ? 'edit' : 'present'}`;
        },

        // 【B】正式環境
        // getImgPath: state => `//${state.InitData.img_path}/tpl/promo/${state.InitData.upload_folder}/${state.RouterName !== 'view' ? 'edit' : 'present'}`,
        // ----------------- ⇧測試環境用 Development dev_01_03⇧ -----------------

        getImgLang: state => state.ImgLang || state.InitData.lang,
        getImgVer: state => state.InitData.data_version,
        getRouterName: state => state.RouterName, // edit 後端, preview 預覽, view 客端
        getRouterLoadingStatus: state => state.RouterLoadingStatus,

        // ----------------- ⇩測試環境用 Development dev_01_04⇩ -----------------
        // 【說明】測試環境模擬 getDevice 所使用，將上下兩段AB程式碼替換即可

        // 【A】測試環境
        getDevice: () => config.user.device,

        // 【B】正式環境
        // getDevice: state => state.Device, // 當前裝置(後台以編輯頁右上方裝置icon切換, 前台以初始api內參數來判斷)
        // ----------------- ⇧測試環境用 Development dev_01_04⇧ -----------------

        getEditWindowData: state => state.EditWindowData, // 後台當前編輯視窗資料
        getEditSetting: state => state.EditSetting, // 後台當前編輯資料
        getEditWindowOkBtnEnabled: state => state.EditWindowOkBtnEnabled, // 編輯確認鈕是否允許點擊

        // ----------------- ⇩測試環境用 Development dev_01_05⇩ -----------------
        // 【說明】js 報錯，暫時 mark 起來，直接解開即可
        //
        //  getSetting: state => _merge(_cloneDeep(state.DefaultSetting), state.InitData.data, state.EditSetting), // 當前顯示的編輯資料
        // ----------------- ⇧測試環境用 Development dev_01_05⇧ -----------------

        getAlert: state => state.Alert // 提示框
    },
    actions: {
        actionSetInitData: ({ commit }, obj = {}) => { commit('setInitData', obj); },
        actionSetRouterLoadingStatus: ({ commit }, str = '') => { commit('setRouterLoadingStatus', str); },

        actionSetDevice: ({ commit }, str = '') => { commit('setDevice', str); },

        actionSetEditWindowData: ({ commit }, obj = {}) => { commit('setEditWindowData', obj); },
        actionSetEditSetting: ({ commit }, obj = {}) => { commit('setEditSetting', obj); },
        actionClearEditSetting: ({ commit }) => { commit('clearEditSetting'); },
        actionSetEditWindowOkBtnEnabled: ({ commit }, Boolean) => { commit('setEditWindowOkBtnEnabled', Boolean); },
        actionSetDefaultSetting: ({ commit }, obj = {}) => { commit('setDefaultSetting', obj); },
        actionSaveThisEdit: ({ commit }) => { commit('saveThisEdit'); },
        actionSetImgLang: ({ commit }, str = '') => { commit('setImgLang', str); },
        actionSetAlert: ({ commit }, obj = {}) => { commit('setAlert', obj); },
    },
    mutations: {
        /* eslint-disable no-param-reassign */
        setInitData: (state, obj) => { state.InitData = { ...state.InitData, ...obj }; },
        setRouterLoadingStatus: (state, bool) => { state.RouterLoadingStatus = bool; },
        setDevice: (state, str) => { state.Device = str; },
        setEditWindowData: (state, obj) => { state.EditWindowData = obj; },

        // ----------------- ⇩測試環境用 Development dev_01_07⇩ -----------------
        // 【說明】js 報錯，暫時 mark 起來，直接解開即可

        // setEditSetting: (state, obj) => { state.EditSetting = { ..._merge(state.EditSetting, obj) }; },
        // ----------------- ⇧測試環境用 Development dev_01_07⇧ -----------------

        clearEditSetting: (state) => { state.EditSetting = {}; },
        setEditWindowOkBtnEnabled: (state, Boolean) => { state.EditWindowOkBtnEnabled = Boolean; },
        setDefaultSetting: (state, obj) => { state.DefaultSetting = obj; },

        // ----------------- ⇩測試環境用 Development dev_01_08⇩ -----------------
        // 【說明】js 報錯，暫時 mark 起來，直接解開即可

        // saveThisEdit: (state) => { state.InitData.data = _merge(state.InitData.data, state.EditSetting); },
        // ----------------- ⇧測試環境用 Development dev_01_08⇧ -----------------

        setImgLang: (state, str) => { state.ImgLang = str; },
        setAlert: (state, obj) => { state.Alert = obj; },
        setLoginStatus: (state, str) => { state.InitData.is_login = str; }
    }
});
```

## fontawesome 設置
###### src/main.js
```javascript
import { library } from '@fortawesome/fontawesome-svg-core';
import { faMapMarkerAlt, faPlusCircle, faBatteryEmpty, faBatteryHalf, faBatteryFull, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faMapMarkerAlt, faPlusCircle, faBatteryEmpty, faBatteryHalf, faBatteryFull, faTimes);
Vue.component('font-awesome-icon', FontAwesomeIcon);
```
> 詳細使用方法請參照 [fontawesome 官方文件](https://www.npmjs.com/package/@fortawesome/vue-fontawesome)

## 刪除預設值
###### package.json
```json=
"eslintConfig": {
    "root": true,
    "env": {
      "node": true
    },
    "extends": [
      "plugin:vue/essential",
      "eslint:recommended"
    ],
    "rules": {},
    "parserOptions": {
      "parser": "babel-eslint"
    }
},
```
> 請刪除以上所示的內容

